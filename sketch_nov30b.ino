/*
 *  Simple hello world Json REST response
  *  by Mischianti Renzo <https://www.mischianti.org>
 *
 *  https://www.mischianti.org/
 *
 */

 // http://10.60.208.117/helloWorld
 
#include "Arduino.h"
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ArduinoJson.h>
#include <ESP8266HTTPClient.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <FS.h>

String ip_address = "";
String subnet = "";
String dns1 = "";
String dns2 = "";
String gateway = "";
String ssid = "SSID_NAME";
String wifiPwd = "SSID_PASSWORD";
String ipHub = "";
String authentication = "";

int buttonState = 0;
int sensorValue0,sensorValue1, prevState0, prevState1  = LOW;
bool movimento0, movimento1 = false;
String currentDate0, currentDate1, statoApertura;

//Week Days
String weekDays[7]={"Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab"};
//Month names
String months[12]={"Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"};

// Define NTP Client to get time
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org");
ESP8266WebServer server(80);

String getValueFromJsonArray(JsonArray arr, String found) {
  for(JsonVariant v : arr) {
    if (v["name"] == found) {
      return (v["value"]);
    }
  }
}

String getAllowedNotification() {

  WiFiClient client;
  HTTPClient http;

    Serial.print("[HTTP] begin...\n");
    // configure traged server and url
    Serial.print(ipHub);
    String host = "http://"+ipHub+"/api/devices/?id=108";
    Serial.print(host);
    http.begin(client, host); //HTTP
    http.addHeader("Content-Type", "application/json");
    http.addHeader("Content-Length", "1024");
    http.addHeader("Authorization", authentication);

    Serial.print("[HTTP] GET...\n");
    // start connection and send HTTP header and body
    int httpCode = http.GET();

    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] GET... code: %d\n", httpCode);

      // file found at server
        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
          //String payload = http.getString();
          // Parse response
          DynamicJsonDocument lev1(2048);
          deserializeJson(lev1, http.getString(), DeserializationOption::NestingLimit(20));
          JsonArray array = lev1["properties"]["quickAppVariables"];
          //Serial.println("Valore ricercato:");
          return getValueFromJsonArray(array, "allowedNotification");
          //deserializeJson(lev2, String(lev1));
          //Serial.println("Payload:");
          //Serial.println(String(lev1["properties"]["quickAppVariables"]));
        }
      } else {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
      }

      http.end();
}

void sendPost(String pushText) {
  WiFiClient client;
  HTTPClient http;

   DynamicJsonDocument doc(512), arr(512);
   String buf;
   JsonArray array = arr.to<JsonArray>();
   array.add(pushText);
   doc["args"] = array;
   doc["delay"] = 1;
   serializeJson(doc, buf);

    Serial.print("[HTTP] begin...\n");
    // configure traged server and url
    String host = "http://"+ipHub+"/api/devices/108/action/sendSignal";
    Serial.print(host);
    http.begin(client, host); //HTTP
    http.addHeader("Content-Type", "application/json");
    http.addHeader("Authorization", authentication);

    Serial.print("[HTTP] POST...\n");
    // start connection and send HTTP header and body
    int httpCode = http.POST(String(buf));

    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] POST... code: %d\n", httpCode);

      // file found at server
      if (httpCode == HTTP_CODE_OK) {
        const String& payload = http.getString();
        Serial.println("received payload:\n<<");
        Serial.println(payload);
        Serial.println(">>");
      }
    } else {
      Serial.printf("[HTTP] POST... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();
}
// Serving Hello world
void getSensorsStatus() {
      String allowedNotification = getAllowedNotification();
      DynamicJsonDocument doc(512), s1(512), s2(512), s3(512);
      String buf;
      s1["type"] = "movimento";
      s1["notification"] = allowedNotification;
      s1["status"] = movimento0;
      s1["dateTime"] = currentDate0;
      doc["sensor1"] = s1;

     /* s2["type"] = "movimento";
      s2["notification"] = allowedNotification;
      s2["status"] = movimento1;
      s2["dateTime"] = currentDate1;
      doc["sensor1"] = s2; */

      s3["type"] = "cancello";
      s3["notification"] = allowedNotification;
      s3["status"] = statoApertura;
      s3["dateTime"] = currentDate1;
      doc["sensor2"] = s3;
      
      serializeJson(doc, buf);
    server.send(200, "text/json", buf);
    }

String getAuthFromFile() {
  Serial.print("Getting Basic Authentication from saved file...");
}

void setAuthToFile() {
  Serial.print("Get BASIC Authentication param and write it into file...");
  String message = "";
  // message += server.args(); //Get number of parameters
  // message += "\n";          //Add a new line
  for (int i = 0; i < server.args(); i++) {
    /* message += "Arg nº" + (String)i + " –> ";   //Include the current iteration value
    message += server.argName(i) + ": ";     //Get the name of the parameter
    message += server.arg(i) + "\n";              //Get the value of the parameter */
    message += server.arg(i);
  };
  SPIFFS.remove("/config.json");
  File configFile = SPIFFS.open("/config.json", "w");
  if (!configFile) {
    Serial.println("Failed to open config file for writing");
  }
  configFile.print(message);
  Serial.println("Config saved");
  server.send(200, "text/plain", message);       //Response to the HTTP request
}

 
// Define routing
void restServerRouting() {
    server.on("/", HTTP_GET, []() {
        buttonState = digitalRead(D7);
        String statoApertura = getState(buttonState);
        server.send(200, "text/html", String("<h1>Welcome in outdoor Baino sensor: </h1><br>") + statoApertura);
    });
    server.on("/say33", HTTP_GET, []() {
        buttonState = digitalRead(D7);
        String statoApertura = getState(buttonState);
        server.send(200, "text/html", statoApertura);
    });
    server.on(F("/helloWorld"), HTTP_GET, getSensorsStatus);
    server.on(F("/auth"), HTTP_POST, setAuthToFile);
}
 
// Manage not found URL
void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void fileHandler() {
  String configuration = "";
  bool success = SPIFFS.begin();
 
  if (success) {
    Serial.println("File system mounted with success");
  } else {
    Serial.println("Error mounting the file system");
    return;
  }
 
  File file = SPIFFS.open("/config.json", "r");
   
  if (!file) {
      Serial.println("Failed to open file for reading");
      return;
    }
    Serial.println("File Content:");
    while (file.available()) {
      configuration = file.readString();
      Serial.write(file.read());
    }

    Serial.println("Singoli parametri di configurazione \n");

    DynamicJsonDocument doc(2048);
    deserializeJson(doc, configuration, DeserializationOption::NestingLimit(20));

    ip_address = doc["ipAddress"].as<String>();
    subnet = doc["subnet"].as<String>();
    dns1 = doc["dns1"].as<String>();
    dns2 = doc["dns2"].as<String>();
    gateway = doc["gateway"].as<String>();
    ssid = doc["ssid"].as<String>();
    wifiPwd = doc["wifiPwd"].as<String>();
    ipHub = doc["ipHub"].as<String>();
    authentication = doc["authentication"].as<String>();
    
    Serial.println(ip_address);
    Serial.println(subnet);
    Serial.println(dns1);
    Serial.println(dns2);
    Serial.println(gateway);
    Serial.println(ssid);
    Serial.println(wifiPwd);
    Serial.println(ipHub);
    Serial.println(authentication);
   
    file.close();
}


 
void setup(void) {
  Serial.begin(115200);
  fileHandler();
  
  // Set your Static IP address
  IPAddress ip_addr, ip_gateway, ip_subnet, ip_dns1, ip_dns2;
  ip_addr.fromString(ip_address);
  IPAddress local_IP(ip_addr);
  //IPAddress local_IP(10, 60, 208, 117);
  // Set your Gateway IP address
  ip_gateway.fromString(gateway);
  IPAddress gateway(10, 10, 0, 1);

  ip_subnet.fromString(subnet);
  IPAddress subnet(255, 0, 0, 0);
  ip_dns1.fromString(dns1);
  IPAddress primaryDNS(8, 8, 8, 8);   //optional
  ip_dns2.fromString(dns2);
  IPAddress secondaryDNS(8, 8, 4, 4); //optional

  Serial.println("IP HUB");
  Serial.println(ipHub);
  pinMode(D7, INPUT_PULLUP);
  pinMode(A0, INPUT);

  WiFi.mode(WIFI_STA);
   // Configures static IP address
  if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
    Serial.println("STA Failed to configure");
  }
  WiFi.begin(ssid, wifiPwd);
  Serial.println("");
 
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  String ip = WiFi.localIP().toString();
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(ip);
  String presentation = "IPADDR:" + ip;
  sendPost(presentation);
 
  // Activate mDNS this is used to be able to connect to the server
  // with local DNS hostmane esp8266.local
  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }
 
  // Set server routing
  restServerRouting();
  // Set not found response
  server.onNotFound(handleNotFound);
  // Start server
  server.begin();
  Serial.println("HTTP server started");

  // Initialize a NTPClient to get time
  timeClient.begin();
  // Set offset time in seconds to adjust for your timezone, for example:
  // GMT +1 = 3600
  // GMT +8 = 28800
  // GMT -1 = -3600
  // GMT 0 = 0
  timeClient.setTimeOffset(3600);
}

int prevState = 1;

String getState(int state) {
  if (state == 0) return "MAG01_CLOSED";
  else return "MAG01_OPEN";
}

String timestamp() {
  timeClient.update();

  unsigned long epochTime = timeClient.getEpochTime();
  struct tm *ptm = gmtime ((time_t *)&epochTime); 
  int monthDay = ptm->tm_mday;
  int currentMonth = ptm->tm_mon+1;
  int currentYear = ptm->tm_year+1900;
  String formattedTime = timeClient.getFormattedTime();

  //Print complete date:
  return String(currentYear) + "-" + String(currentMonth) + "-" + String(monthDay) + " " + String(formattedTime);
}

void loop(void) {
  
  server.handleClient();
  sensorValue0 = digitalRead(D1);
  // sensorValue1 = digitalRead(D2);
  buttonState = digitalRead(D7);
  //Serial.println(buttonState);
  
  // Logica Sensore Magnetron
  if (buttonState != prevState) {
    prevState = buttonState;
    statoApertura = getState(buttonState);
    currentDate1 = timestamp();
    sendPost(statoApertura);
    Serial.println(statoApertura);
  }

  // Logica Sensore Crepuscolare 1
  if (sensorValue0 == HIGH) {
    if (sensorValue0 != prevState0) {
      prevState0 = sensorValue0;
      movimento0 = true;
      sendPost("PIR01");
      Serial.println("Movimento Sensore 1 Rilevato");
      currentDate0 = timestamp();
      Serial.println(timestamp());
    } else {
      movimento0 = false;
      //Serial.println("Nessun Movimento");
    }
  } else {
    prevState0 = sensorValue0;
    movimento0 = false;
  }

  // Logica Sensore Crepuscolare 2
  /* if (sensorValue1 == HIGH) {
    if (sensorValue1 != prevState1) {
      prevState1 = sensorValue1;
      movimento1 = true;
      sendPost();
      Serial.println("Movimento Sensore 2 Rilevato");
      currentDate1 = timestamp();
      Serial.println(timestamp());
    } else {
      movimento1 = false;
      //Serial.println("Nessun Movimento");
    }
  } else {
    prevState1 = sensorValue1;
    movimento1 = false;
  } */
  // Serial.println(buttonState);
  delay(1000);
}