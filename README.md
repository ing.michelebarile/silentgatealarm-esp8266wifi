# silentGateAlarm - ESP8266Wifi

This script allows the reception about the status of a personal home gate (Open / Closed). In addition, it provides data from two PIR sensors connected to the ESP8266 Wifi card.

![drawing](img/2.jpeg)
## Getting started

To make it easy for you to get started with this script, here's a list of recommended next steps.

- Install Arduino IDE
- Plug in ESP8266
- Check into device manager if driver are all be installed
- Open Arduino IDE
- Go to File -> Preferences and add the following json into Additional Boards Manager URLs:

    ![drawing](img/json%20file%20preferences.png)
- Go to Tool -> Board -> Board Manager and select as the following screenshot

    ![drawing](img/board-manager-ide.png)
- Select the following Board ad screenshot

    ![drwaing](img/configurazione%20scheda%20ide.png)

## Startup configuration

A first configuration setting can be send to board by the following POST request:

http://[IP-ADDRESS]/auth

### Payload
```
{
    "ipAddress": "127.0.0.1",
    "subnet": "255.255.255.0",
    "dns1": "8.8.8.8",
    "dns2": "8.8.4.4",
    "gateway": "127.0.0.1",
    "ssid": "name of wifi ssid",
    "wifiPwd": "password wifi",
    "ipHub": "127.0.0.1",
    "authentication": "Basic AHAydgsyfdsbbf=="
}
```
then press the RESET button on board to read the new config.json file written

**WARNING**: Do not change **ssid** and **wifiPwd** if you cannot use a <ins>real exisisting wifi-network</ins>. In this case you must reload the firmware via USB.

## How it works

- ESP8266-Wifi is configured as WIfi Station (STA mode)

- ESP8266-WIfi once received chainging of status can send data at another system via *HTTP Request* as follow http://[IP-ADDRESS]/api/devices/108/action/sendSignal

- The script provides a REST API interface at following URL: http://[IP-ADDRESS]

### API endpoint

| Endpoint URL | Description | Format
| ------------- | ------------- | ------------- |
| /  | HTML Welcome interface | text/html
| /helloWorld | provides all data about connected sensors with own timestamps | json
| /say33 | (used as WhatchDog) print the magnetic sensor status as a String | text/html
| /auth | POST type request with the above payload. | text/plain

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

